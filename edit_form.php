<?php
 
class block_programming_latest_ac_edit_form extends block_edit_form {
 
    protected function specific_definition($mform) {
        // Fields for editing HTML block title and contents.
        $mform->addElement('header', 'configheader', get_string('blocksettings', 'block'));

        $mform->addElement('text', 'listhowmany', get_string('showhowmanyonlist', 'block_programming_latest_ac'));
        $mform->setDefault('listhowmany',15);
        $mform->setType('listhowmany',PARAM_INT);
        $mform->addElement('text', 'perpageonfulllist', get_string('perpageonfulllist', 'block_programming_latest_ac'));
        $mform->setDefault('perpageonfulllist',20);
        $mform->setType('perpageonfulllist',PARAM_INT);
    }

    function set_data($defaults) {
        parent::set_data($defaults);

    }

}
